// # Calctail.js
// Copyright © 2022 Mike Charlton
// The MIT License (MIT)
// See LICENSE in main project for more information.

// Code that works with the DOM to do some calculations for cocktails.
// Currently it calculates amounts of ingredients for making super juice.

// ## Bootstrap
// Just a couple functions that the test framework needs
// These are lets because we'll redefine them with tests later

// zip(['a', 'b', 'c'][1, 2, 3]) === [['a', 1], ['b', 2], ['c', 3]]
let zip = as => bs => // abs
  Array.from(as).map((a, i) => [a, bs[i]])
;

// curry(a => b => c => a + b + c, [1, 2, 3]) ===
//   (a => b => c => a + b + c)(1)(2)(3)
let curry = f => as => // b
  as.reduce((g, a) => g(a), f)
;

// Maybe implemented with an Array.
class Maybe extends Array { }
Maybe.prototype.bind = Maybe.prototype.flatMap

// some(a) === [a]
// It indicates that a value exists
let some = a => // Ma
  Maybe.from([a])
;

// none() === []
// It indicates that a value does not exist
let none = () => // Ma
  Maybe.from([])
;

// Namespace some code.
// Example:
//   const num = namespace(
//     () => {
//       const one = 1;
//       return { inc: x => x + one };
//     }
//   );
//   num.inc(5);
let namespace = f =>
  f()
;


// ## Test Framework

const test = namespace(
  () => {
    // Runs the test with the values
    // Returns some(error) if the test failed
    // Otherwise returns none()
    const assert = test => values => // Merror
      curry(test)(values)
        ? none() 
        : some(values.join(' : ').toString())
    ;

    // Returns a function that always returns stub_val
    // However it remembers the parameter for the last invocation
    const mock = stub_val => {
      const f = actual => (_ => stub_val)(f.last = actual);
      return f;
    };

    // Note: eql and to_s can theoretically exhaust stack space
    //       practically, it ain't gunna happen.
    const eql = actual => expected => // boolean
      type(expected) === type(actual) &&
        type(expected).eql(actual)(expected)
    ;

    const to_s = a => // string
      type(a).to_s(a)
    ;

    const type_name = a => // string
      typeof(a) === 'object'
        ? a.constructor.name
        : typeof(a)
    ;

    const type = a => // Variant
      [StringType, ArrayType, DictType, DefaultType].find(x => x.is(a))
    ;

    // Assertions on strings
    const StringType = {
      is: a => // boolean
      type_name(a) === 'String',

      eql: a1 => a2 => // boolean
      a1 === a2,

      to_s: a => //string
      `'${a}'`
    };

    // Assertions on Arrays
    const ArrayType = {
      is: a => // boolean
      Array.isArray(a),

      eql: a1 => a2 => // boolean
      a1.length === a2.length &&
      zip(a1)(a2).every(curry(eql)),

      to_s: a => // string
      `[${a.map(to_s).join(', ')}]`,
    };

    // Assertions on Dictionaries
    const DictType = {
      is: a => // boolean
      type_name(a) === 'Object',

      eql: a1 => a2 => // boolean
      ArrayType.eql(Object.entries(a1))(Object.entries(a2)),

      to_s: a => // string
      `{${Object.entries(a).map(x => x[0] + ': ' + to_s(x[1])).join(', ')}}`,
    };

    // Assertions on everything else
    const DefaultType = {
      is: _ => // boolean
      true,

      eql: a1 => a2 => // boolean
      a1 === a2,

      to_s: a => //string
      `${a}`
    };

    return {
      assert: assert,
      eql: eql,
      mock: mock
    };
  }
);

// Typical usage:
//   example ('Example name')
//     ( code_goes_here() ).returns(value)
// Or:
//   example ('Example name')
//     ( code_goes_here() ).satisfies(function)
const example = label => actual => // Example
  namespace(
    () => {
      const { assert, eql } = test;

      const returns = expected => // [label, Merror]
        [label, assert(eql) ([actual, expected])]
      ;

      const satisfies = test =>  // [label, Merror]
        [label, assert(test) ([actual])]
      ;

      return {returns: returns, satisfies: satisfies};
    }
  )
;

// Define a function along with tests
// Example:
//   const inc = def (
//     a => // a
//
//     x + 1
//   ) ( () => [
//     example ('adds 1 to a number') (
//       inc(5)
//     ).returns( 6 )
//   ]);
const def = f => tests_f => // f
  (_ => f)(
    f.tests = format => tests_f().map(
      tuple => format(tuple[0])(tuple[1])
    )
  )
;

// Take an array of functions defined with def()
// Return a function that takes a formatter and returns the test results
// Here is an example formatter:
// label => Merror => // string
//   is_none(Merror)
//     ? `PASS - ${label}`
//     : `FAIL (${Merror.toString()}) - ${label}
// ;
const test_runner = fs => format => //strings
  fs.flatMap(f => f.tests(format))
;

// ## Bootstrap tests
// Implementation of these functions are at the top of the file

curry = def(curry) ( () => [
  example ('curry applies paramters from an array one at a time') (
    curry(x => y => z => (x - y) * z) ([5, 3, 2])
  ).returns( 4 ),
]);

zip = def(zip) ( () => [
  example ('zip mixes 2 arrays') (
    zip([1, 2, 3])([4, 5, 6])
  ).returns( [[1, 4], [2, 5], [3, 6]] ),

  example ('zip discards extra elements in the second array') (
    zip([1, 2])([4, 5, 6])
  ).returns( [[1, 4], [2, 5]] ),

  example ('zip uses undefined to pad out the second array') (
    zip([1, 2, 3])([4, 5])
  ).returns( [[1, 4], [2, 5], [3, undefined]] ),
]);

const bootstrap_tests = // format => strings
  test_runner([curry, zip])
;


// ## FP Tools

const tap = def(
  a => f => // a

  (_ => a)(f(a))
) ( () => [
  example ('tap applies the value to the function always returning the original') (
    tap(4)(x => x + 5)
  ).returns( 4 ),

  example ('tap can mutate the original value') (
    tap([4])(x => x[0] = 12)
  ).returns( [12] ),
]);

const mock_log = test.mock(undefined);

const dbg = def(
  (a, log = console.log) => // a + console_effect

  tap(a)(log)
) ( () => [
  example ('dbg outputs the value to the log') (
    dbg(42, mock_log)
  ).satisfies( _ => test.eql(mock_log.last)(42) ),

  example ('dbg returns the value') (
    dbg(42, mock_log)
  ).returns( 42 ),
]);

const compact = def(
  as => // as

  as.filter(a => a !== undefined && a !== null)
) (() => [
  example ('compact removes undefined entries in an array') (
    compact([1, undefined, 2])
  ).returns( [1, 2] ),
  example ('compact removes null entries in an array') (
    compact([1, null, 2])
  ).returns( [1, 2] ),
  example ('compact does not remove 0 entries in an array') (
    compact([1, 0, 2])
  ).returns( [1, 0, 2] )
]);

const fp_tests = // format => strings
  test_runner([tap, dbg, compact])
;


// ## Maybe monad.

some = def(some)( () => [
  example ('some wraps a value') (
    some(42)
  ).returns( [42] ),
  example ('some acts like a functor') (
    some(42).map(num => num + 5)
  ).returns( [47] ),
  example ('some acts like a monad') (
    some(42).bind(num => some(num + 5))
  ).returns( [47] ),
]);

none = def(none)( () => [
  example ('none wraps nothing') (
    none()
  ).returns( [] ),
  example ('some acts like a functor') (
    none().map(num => num + 5)
  ).returns( [] ),
  example ('some acts like a monad') (
    none().bind(num => some(num + 5))
  ).returns( [] ),
]);

const is_none = def(
  Ma => // boolean

  Ma.length === 0
)( () => [
  example ('is_none returns true if it is none') (
    is_none(none())
  ).returns( true ),
  example ('is_none returns false if it is some') (
    is_none(some(42))
  ).returns( false ),
]);

const maybe = def(
  f => a => // Ma

  some(a).filter(f)
)( () => [
  example ('maybe returns some if filter true') (
    maybe(x => x % 2 === 0)(6)
  ).returns( some(6) ),
  example ('maybe returns none if filter false') (
    maybe(x => x % 2 === 0)(5)
  ).returns( none() ),
]);

const sequence = def(
  arrM => // Marr

  arrM.reduce(
    (Marr, Ma) => (
      is_none(Marr) || is_none(Ma)
      ? none()
      : tap(Marr) (Marr => Marr[0].push(Ma[0]))
    ), some([])
  )
) ( () => [
  example ('sequence takes an array of Maybes and returns a Maybe with an Array') (
    sequence([some(42), some(27), some(3)])
  ).returns( some([42, 27, 3]) ),
  example ('sequence returns some([]) if the array is empty') (
    sequence([])
  ).returns( some([]) ),
  example ('sequence returns none() if any element is none()') (
    sequence([some(42), none(), some(3)])
  ).returns( none() ),
]);

const monad_tests = // format => strings
  test_runner([some, none, is_none, maybe, sequence])
;

// ## Remedies for various JS Problems

const not_nan = def(
  // a => Mnum

  maybe(a => !isNaN(a))
)( () => [
  example ('not_nan returns some(a) if a is a number') (
    not_nan(42)
  ).returns( some(42) ),
  example ('not_nan returns none() if a is NaN') (
    not_nan(NaN)
  ).returns( none() ),
]);

const try_num = def(
  a => // Mnum

  not_nan(parseFloat(a))
)( () => [
  example ('try_num returns some(a) if a is a number') (
    try_num(42)
  ).returns( some(42) ),
  example ('try_num returns some(num) if a can be converted to a number') (
    try_num('42')
  ).returns( some(42) ),
  example ('try_num returns none() if a can not be converted to number') (
    try_num('fortytwo')
  ).returns( none() ),
  example ('try_num can convert some(num) to a number') (
    try_num(some(42))
  ).returns( some(42) ),
  example ('try_num can convert some(string) to a number') (
    try_num(some('42'))
  ).returns( some(42) ),
]);

// Returns some(a) if a is not null, otherwise none().
const is_set = def(
  // a => Ma

  maybe(a => a !== null && a !== undefined)
)( () => [
  example ('is_set returns some(a) if a is not null') (
    is_set(42)
  ).returns( some(42) ),
  example ('is_set returns none() if a is null') (
    is_set(null)
  ).returns( none() ),
  example ('is_set returns none() if a is undefined') (
    is_set(undefined)
  ).returns( none() ),
]);
const js_tests = // format => strings
  test_runner([not_nan, try_num, is_set])
;

// ## DOM functions for creating fakes for tests
const fake_element = value => mock_listener => ({
  value: value,
  addEventListener: (type, callback) => mock_listener([type, callback]),
  setAttribute: (_name, _value) => undefined,
  innerHTML: '',
  outerHTML: '',
});

const fake_document = map =>
  ({
    getElementById: id => map[id] || null,
    createElement: _type => fake_element(null)(_ => undefined),
  })
;

// ## DOM utitilies
const get_element = def(
  doc => id => //Melement

  is_set(doc.getElementById(id))
) ( () => {
  const listener = test.mock(undefined);
  const element = fake_element(42)(listener);
  const doc = fake_document({ fake_id: element })

  return [
    example ('get_element returns some(element) from the document if it exists') (
      get_element(doc)('fake_id')
    ).returns(some(element)),

    example ('get_element returns none if the element does not exist') (
      get_element(doc)('not_there')
    ).returns(none()),
  ];
});

// Listen to input events on the element specified by the id.
// Call the callback when an event occurs.
const capture_input = def(
  doc => id => callback => // Melement + dom_effect

  get_element(doc)(id).map(
    element => tap(element) (
      input => input.addEventListener('input', callback)
    )
  )
) ( () => {
  const callback = _ => null;
  const listener = test.mock(undefined);
  const element = fake_element(42)(listener);
  const doc = fake_document({ fake_id: element })

  return [
    example ('capture_input returns the element') (
      capture_input(doc)('fake_id')(callback)
    ).returns(some(element)),

    example ('capture_input listens to input events on the element specified by the id') (
      capture_input(doc)('fake_id')(callback)
    ).satisfies(_ => test.eql(listener.last)(['input', callback])),
  ];
});

// Get the element at the id and return its value.
const value = def(
  doc => id => // Mstring

  get_element(doc)(id).bind(
    el => is_set(el.value)
  )
) ( () => {
  const listener = test.mock(undefined);
  const full_element = fake_element(42)(listener);
  const null_element = fake_element(null)(listener);
  const undefined_element = fake_element(undefined)(listener);
  const doc = fake_document({
    fortytwo: full_element,
    null_id: null_element,
    undefined_id: undefined_element,
  });

  return [
    example ('value returns the value in the specified element') (
      value(doc)('fortytwo')
    ).returns(some(42)),
    example ('value returns none if the element is null') (
      value(doc)('null_id')
    ).returns(none()),
    example ('value returns none if the element is undefined') (
      value(doc)('undefined_id')
    ).returns(none()),
  ];
});

// Set the value of the element at an id if it exists.
const set_value = def(
  doc => id => value => // value

  tap(value) (
    value => get_element(doc)(id).bind(
      el => el.value = value.toString()
    )
  )
) ( () => {
  const listener = test.mock(undefined);
  const zero_element = fake_element(0)(listener);
  const doc = fake_document({
    zero: zero_element,
  });

  return [
    example ('set_value sets the value of the element') (
      set_value(doc)('zero')(42)
    ).satisfies(_ => test.eql(value(doc)('zero'))(some('42'))),
    example ('set_value does nothing if the element does not exist') (
      set_value(doc)('not_there')(42)
    ).returns(42),
  ];
});

const dom_tests = // format => strings
  test_runner([get_element, capture_input, value, set_value])
;

// Take an array of ids and a function with that many parameters.
// Fetch the values from the elements at the ids as numbers and pass them to f.
// Returns some(f(values)) if there are no problems.
// Otherwise returns none().
const num_values = doc => ids => f =>
  sequence(ids.map(x => try_num(value(doc)(x)))).map(
    curry(f)
  )
;

// Inserts the html in the page at the given id
const publish = doc => id => html => // Melement + page_effect
  tap(get_element(doc)(id)) (
    Melement => Melement.bind(
      el => el.innerHTML = html
    )
  )
;

// Return a DOM element of the given type
const element = doc => (type, attrs = {}) => // element
  tap(doc.createElement(type)) (
     el => Object.entries(attrs).forEach(attr => el.setAttribute(...attr))
  )
;

// Create an HTML element with no contents
const entry = doc => (type, attrs = {}) => // html
  element(doc)(type, attrs).outerHTML
;

// Create an HTML container element of the given type
// and fill it with the given html
const container = doc => (type, attrs = {}) => (...html_list) => // html
  tap(element(doc)(type, attrs)) (
    el => el.innerHTML = html_list.join("\n")
  ).outerHTML
;

// create a div Element and fill it with the given html
const div = doc => (...html_list) => // html
  container(doc)('div')(...html_list)
;

const li = doc => (...html_list) => // html
  container(doc)('li')(...html_list)
;

const ul = doc => strings => // html
  container(doc)('ul') (
    strings.map(
      string => li(doc)(string)
    ).join("\n")
  )
;

const add_class = klass => attrs => // attrs
  ({attrs, ...{'class': compact([klass, attrs['class']]).join(' ')}})
;

const contents = doc => attrs => (...html_list) => // html
  container(doc)('div', add_class('contents')(attrs))(...html_list)
;

const drop = doc => (type, attrs = {}) => heading => (...html_list) => // html
  container(doc)('label', {'class': 'drop'})(
    entry(doc)('input', {'type': 'checkbox', 'class': 'collapse'}),
    container(doc)(type, {'class': 'heading'})(heading),
    contents(doc)(attrs)(...html_list)
  )
;

// ## Super Juice Calculator code

// Update the input value at id,
// by running f with the amount and values provided.
// Returns some(amount) if there were no problem.
// Otherwise returns none().
const update_from = doc => amount => values => id => f => // Mamount + dom_effect
  num_values(doc)(values)(
    p1 => p2 => tap(amount) (
      _ => set_value(doc)(id) (
        f(p1)(p2).toFixed(1)
      )
    )
  )
;

const recipe = doc => namespace(
  () => {
    // Calculate the water amount from the rind amount.
    // Update the input value for water amount in the process.
    // Returns some(water_amount) if there were no problem.
    // Otherwise returns none().
    const water = doc => rind_amount => // Mwater_amount + dom_effect
      num_values(doc)(['rind-parts', 'water-parts'])(
        rind_parts => water_parts => (
          tap((rind_amount / rind_parts) * water_parts) (
            water_amount => set_value(doc)('water')(
              water_amount.toFixed(1)
            )
          )
        )
      )
    ;

    // Calculate the rind amount from the water amount.
    // Update the input value for rind amount in the process.
    // Returns some(water_amount) if there were no problem.
    // Otherwise returns none().
    const rind = doc => water_amount => // Mrind_amount + dom_effect
      update_from(doc)(water_amount)(['rind-parts', 'water-parts'])('rind') (
        rind_parts => water_parts => (
          (water_amount / water_parts) * rind_parts
        )
      )
    ;

    // Calculate the total acid amount give the rind amount.
    // Return some(acid_amount) if there were no problems.
    // Otherwise returns none().
    // Does not update anything in the DOM.
    const acid = doc => water_amount => // Macid_amount
      try_num(value(doc)('acid-percent')).map(
        acid_percent => water_amount * (acid_percent / 100)
      )
    ;

    // Calculate the citric acid amount from the total acid amount.
    // Update the input value for citric amount in the process.
    // Returns some(acid_amount) if there were no problem.
    // Otherwise returns none().
    const citric = doc => acid_amount => // Macid_amount + dom_effect
      update_from(doc)(acid_amount)(['citric-parts', 'malic-parts'])('citric')(
        citric_parts => malic_parts => (
          (acid_amount / (citric_parts + malic_parts)) * citric_parts
        )
      )
    ;

    // Calculate the malic acid amount from the total acid amount.
    // Update the input value for malic amount in the process.
    // Returns some(acid_amount) if there were no problem.
    // Otherwise returns none().
    const malic = doc => acid_amount => // Macid_amount + dom_effect
      update_from(doc)(acid_amount)(['citric-parts', 'malic-parts'])('malic')(
        citric_parts => malic_parts => (
          (acid_amount / (citric_parts + malic_parts)) * malic_parts
        )
      )
    ;

    return {
      from_rind: Mrind_amount =>
        Mrind_amount.bind(water(doc))
          .bind(acid(doc))
          .bind(citric(doc))
          .bind(malic(doc)),
      from_water: Mwater_amount =>
        Mwater_amount.bind(rind(doc))
          .bind(acid(doc))
          .bind(citric(doc))
          .bind(malic(doc)),
    };
  }
);

const watch_input = doc => namespace(
  () => {
    const event_num = e => // Mnum
      try_num(is_set(e.target.value))
    ;

    // Monitor rind changes and update other values
    capture_input(doc)('rind')(
      e => recipe(doc).from_rind(event_num(e))
    );

    // Monitor water changes and update other values
    capture_input(doc)('water')(
      e => recipe(doc).from_water(event_num(e))
    );
  }
);

// ## Tests

const publish_test_results = doc => namespace(
  () => {
    const format = label => Merror => // string
      is_none(Merror)
        ? `<span class="pass">PASS</span> - ${label}`
        : `<span class="fail">FAIL (${Merror.toString()})</span> - ${label}`
    ;

    publish(doc)('tests')(
      drop(doc)('h2', {'class': 'test-categories'})('Tests')(
        drop(doc)('h3', {'class': 'tests'})('Test Framework Tests')(ul(doc)(bootstrap_tests(format))),
        drop(doc)('h3', {'class': 'tests'})('FP Tests')(ul(doc)(fp_tests(format))),
        drop(doc)('h3', {'class': 'tests'})('Monad Tests')(ul(doc)(monad_tests(format))),
        drop(doc)('h3', {'class': 'tests'})('JS Tests')(ul(doc)(js_tests(format))),
        drop(doc)('h3', {'class': 'tests'})('DOM Tests')(ul(doc)(dom_tests(format))),
      )
    );
  }
);

const log_test_results = log => namespace(
  () => {
    const format = label => Merror => // string
      is_none(Merror)
        ? `PASS - ${label}`
        : `FAIL (${Merror.toString()}) - ${label}`
    ;

    [
      bootstrap_tests,
      fp_tests,
      monad_tests,
      js_tests,
      dom_tests,
    ].reduce(
      (results, tests) => results.concat(tests(format)), []
    ).forEach(x => log(x))
  }
);

const doc = typeof(document) === 'undefined'
  ? fake_document({})
  : document
;

watch_input(doc)
publish_test_results(doc)
log_test_results(console.log)
