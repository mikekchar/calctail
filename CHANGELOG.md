# Changelog

This changelog is a bit different than others.  It contains a
TODO list at the top and the changelog is actually at the bottom.

## TODO

  - [ ] Write tests for everything
  - [ ] Calculate all values from Citric update
  - [ ] Calculate all values from Malic update
  - [ ] Change order of Rind and Water in calculator.
        Everything should be keyed off rind by default.
  - [ ] Recalculate everything from Rind if Ratios change
  - [ ] Clear all recipe values when any value is empty
  - [ ] Capture errors in things that can fail
  - [ ] Output captured errors
  - [ ] Allow calculation without only Citric acid

## Changes

  - [X] Replace `not_null` is `is_set` for bot `null` and `undefined`
  - [X] Should `value` return `none()` if the value is `null` or `undefined`?
    Yes
  - [X] Add a crappy console test runner
    I'm sick of having to inspect the tests in the browser all the time
    - [X] pass `document` to everything that uses it
    - [X] implement `createElement` in `fake_document`
      - [X] do something with `type`
    - [X] create `setAttribute` on `fake_element` with no implementation
    - [X] create `innerHTML` on `fake_element` with no implementation
    - [X] if `document` is not set, use a `fake_document` for input
    - [X] if `document` is not set, use a `fake_document` for tests
    - [X] log the test results to the console
  - [X] Add dark mode CSS
  - [X] User variables for colors in the CSS
  - [X] Use test.eql properly with 2 parameters
  - [X] Add let blocks to test framework (ha ha ha ha)
        In the end it was fine to just use the block I had
  - [X] Refactor DOM functions to explicitly take `document`
  - [X] Be able to add new test sections without changing HTML
    - [X] Integrate `contents` with `container`
          In other words, allow containers to accept a list of html
    - [X] merge `contents` CSS class into `drop`
    - [X] Allow `drop` to specify the label heading type
  - [X] Only one implementation of curry and zip
  - [X] Output code for expectation in satisfies
  - [X] Integrate def() and stub() into test framework
  - [X] Fix stub() so that it contains the expectation properly
  - [X] Round values in the calculator to 1 decimal
  - [X] Calculate all values from Water update
  - [X] implement sequence
  - [X] Calculate Citric and Malic acid properly
  - [X] Calculate water and acid from rind amount
  - [X] Rough in complete tables
  - [X] Use a maybe monad for extracting data
  - [X] Add text input fields for amounts
  - [X] Improve the CSS
        It's not fantastic, but it will do for now.
  - [X] Create a frame for the calculator
  - [X] Get JS running up on the page
  - [X] Get CI/CD Pipeline set up to host page
  - [X] Add a Changelog
  - [X] Add a License file
  - [X] Add Basic page with a license
  - [X] Add a README
